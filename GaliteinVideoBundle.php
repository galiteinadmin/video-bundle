<?php

/*
 * This file is part of the Novo SGA project.
 *
 * (c) Rogerio Lino <rogeriolino@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Galitein\VideoBundle;

use Galitein\Module\BaseModule;

class GaliteinVideoBundle extends BaseModule
{
    public function getIconName()
    {
        return 'video';
    }

    public function getDisplayName()
    {
        return 'module.name';
    }

    public function getHomeRoute()
    {
        return 'galitein_video_index';
    }
}
