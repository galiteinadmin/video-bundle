<?php

/*
 * This file is part of the Novo SGA project.
 *
 * (c) Rogerio Lino <rogeriolino@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Galitein\VideoBundle\Controller;

use Exception;
use Novosga\Entity\Contador;
use Novosga\Entity\Local;
use Novosga\Entity\Servico;
use Novosga\Entity\ServicoUnidade;
use Novosga\Entity\ServicoUsuario;
use Novosga\Entity\Usuario;
use Novosga\Http\Envelope;
use Novosga\Service\AtendimentoService;
use Novosga\Service\FilaService;
use Novosga\Service\UsuarioService;
use Novosga\Service\ServicoService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * DefaultController.
 *
 * Controlador do módulo de configuração da unidade
 *
 * @author Rogerio Lino <rogeriolino@gmail.com>
 */
class DefaultController extends Controller
{
    const DOMAIN        = 'NovosgaVideoBundle';
    const DEFAULT_SIGLA = 'A';

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/", name="novosga_video_index", methods={"GET"})
     */
    public function index(Request $request)
    {
        $search = $request->get('q');

        $query = $this
            ->getDoctrine()
            ->getManager()
            ->createQueryBuilder()
            ->select('e')
            ->from(Entity::class, 'e')
            ->getQuery();

        $currentPage = max(1, (int) $request->get('p'));

        $adapter    = new \Pagerfanta\Adapter\DoctrineORMAdapter($query);
        $pagerfanta = new \Pagerfanta\Pagerfanta($adapter);
        $view       = new \Pagerfanta\View\TwitterBootstrap4View();

        $pagerfanta->setCurrentPage($currentPage);

        $path = $this->generateUrl('novosga_users_index');
        $html = $view->render(
            $pagerfanta,
            function ($page) use ($request, $path) {
                $q = $request->get('q');

                return "{$path}?q={$q}&p={$page}";
            },
            [
                'proximity'    => 3,
                'prev_message' => '←',
                'next_message' => '→',
            ]
        );

        $videos = $pagerfanta->getCurrentPageResults();

        return $this->render('@NovosgaVideo/default/index.html.twig', [
            'videos'  => $videos,
            'paginacao' => $html,
        ]);
    }


}
